from fastapi import APIRouter, Depends, status
from .. import schemas, database, oauth2
from sqlalchemy.orm import Session
from ..repository import blog
from typing import List

router = APIRouter(
    tags=["Blogs"]
)


@router.post("/blog", status_code=status.HTTP_201_CREATED)
def create_blog(request: schemas.Blog, db: Session = Depends(database.get_db),
                get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.create_blog(request, db)


@router.get("/blog/{id}", response_model=schemas.ShowBlog)
def get_blog(id: int, db: Session = Depends(database.get_db),
             get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.get_blog(id, db)


@router.get("/blogs", response_model=List[schemas.Get_all_blogs])
def get_all_blogs(db: Session = Depends(database.get_db),
                  get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.get_all_blogs(db)


@router.put("/blog/{id}")
def update_blog(id: int, request: schemas.Blog, db: Session = Depends(database.get_db),
                get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.update_blog(id, request, db)


@router.delete("/blog/{id}")
def delete_blog(id: int, db: Session = Depends(database.get_db),
                get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.delete_blog(id, db)
