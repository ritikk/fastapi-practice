from .. import models
from passlib.context import CryptContext
from fastapi import HTTPException, status

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def create_user(request, db):
    existing_user = db.query(models.User).filter(models.User.email == request.email).first()
    if existing_user:
        return "The email already exists in the database"

    hashed_password = pwd_context.hash(request.password)
    new_user = models.User(name=request.name, email=request.email, password=hashed_password)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def show_user(id, db):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User with the id {id} not found")

    return user


def show_all_user(db):
    users = db.query(models.User).all()
    if users == []:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The database is empty. Kindly create a user first")
    return users
