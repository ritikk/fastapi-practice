from .. import models
from fastapi import HTTPException, status


def create_blog(request, db):
    new_blog = models.Blog(title=request.title, body=request.body, user_id=1)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog


def get_blog(id, db):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"The id {id} is not present in the database")
    return blog


def get_all_blogs(db):
    blogs = db.query(models.Blog).all()
    if blogs == []:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="The database is empty. Kindly create some blogs first")
    return blogs


def update_blog(id, request, db):
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The blog with id {id} not found in the database")
    blog.update(dict(title=request.title, body=request.body))
    db.commit()
    return "Updated Successfully"


def delete_blog(id, db):
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The blog with id {id} is not in the database")
    blog.delete(synchronize_session=False)
    db.commit()
    return f"Deletion of id {id} is completed"